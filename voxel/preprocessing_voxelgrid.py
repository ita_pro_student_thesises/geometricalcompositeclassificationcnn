import trimesh as tm
import numpy as np
import os
import random
import pickle

DATASET = 'VANILLA'  # 'VANILLA' OR 'ADJUSTED'

if DATASET == 'VANILLA':
    DATADIR = "../STL_files/data"
elif DATASET == 'ADJUSTED':
    DATADIR = "../STL_files/data_adjusted"
else:
    print('Dataset not specified!')


def stl_to_voxel(train_test):
    CATEGORIES = os.listdir(DATADIR)
    data = []
    assignment = []
    swap01 = 0  # counter to check balanced number of rotations
    swap02 = 0  # counter to check balanced number of rotations

    for category in CATEGORIES:
        path = os.path.join(DATADIR, category, train_test)
        class_number = CATEGORIES.index(category)
        counter = 0
        for obj in os.listdir(path):
            counter += 1
            mesh = tm.load(os.path.join(path,obj))
            voxel = mesh.voxelized(mesh.extents[np.argmax(mesh.extents)]/62)  #standard Voxel size: 64x64x64 (PointGrid)
            voxel_bool = voxel.matrix
            voxel_binary = np.zeros([64, 64, 64]).astype("int8")  #save as int8 for minimum RAM usage

            for i in range(voxel_bool.shape[0]):
                for j in range(voxel_bool.shape[1]):
                    for k in range(voxel_bool.shape[2]):
                        if voxel_bool[i, j, k] == True:
                            voxel_binary[i, j, k] = 1

            if counter % 2 == 0 and counter % 3 != 0:  #rotate voxels to prevent overfit
                voxel_binary = voxel_binary.swapaxes(0, 1)
                print("swap:0,1")
                swap01 += 1
            if counter % 3 == 0:
                voxel_binary = voxel_binary.swapaxes(0, 2)
                print("swap:0,2")
                swap02 += 1

            data.append([voxel_binary, class_number])
            assignment.append(obj)
    return data, assignment


def save_data(data, assignment, train_test):

    temp = list(enumerate(data))
    random.shuffle(temp)
    indices, data = zip(*temp)
    X = []
    y = []
    assignment = [indices, assignment]

    for features, label in data:
        X.append(features)
        y.append(label)

    if DATASET == 'VANILLA':
        X_filename = "./data/X_" + train_test + ".pickle"
        y_filename = "./data/y_" + train_test + ".pickle"
        assignment_filename = "./data/assignment_" + train_test + ".pickle"
    elif DATASET == 'ADJUSTED':
        X_filename = "./data/X_" + train_test + "_adjusted.pickle"
        y_filename = "./data/y_" + train_test + "_adjusted.pickle"
        assignment_filename = "./data/assignment_" + train_test + "_adjusted.pickle"

    pickle_out = open(X_filename,"wb")
    pickle.dump(X, pickle_out)
    pickle_out.close()

    pickle_out = open(y_filename,"wb")
    pickle.dump(y, pickle_out)
    pickle_out.close()

    pickle_out = open(assignment_filename, "wb")
    pickle.dump(assignment, pickle_out)
    pickle_out.close()


def get_categories():
    categories = os.listdir(DATADIR)
    return categories


training_data, train_assignment = stl_to_voxel("train")  # create training dataset
test_data, test_assignment = stl_to_voxel("test")  # create test dataset
save_data(training_data,train_assignment, "train")
save_data(test_data, test_assignment, "test")
