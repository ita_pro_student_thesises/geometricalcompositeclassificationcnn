from model import VoxelModel
import time
import numpy as np
import os
import pickle
from tensorflow.keras.callbacks import TensorBoard


DATASET = 'VANILLA'  # 'VANILLA' OR 'ADJUSTED'

if DATASET == 'VANILLA':
    pickle_in = open("data/X_train.pickle","rb")
    X_train = pickle.load(pickle_in)
    pickle_in = open("data/y_train.pickle","rb")
    y_train = pickle.load(pickle_in)
elif DATASET == 'ADJUSTED':
    pickle_in = open("data/X_train_adjusted.pickle", "rb")
    X_train = pickle.load(pickle_in)
    pickle_in = open("data/y_train_adjusted.pickle", "rb")
    y_train = pickle.load(pickle_in)
else:
    print('Dataset not specified!')

X_train = np.array(X_train).reshape(-1, 64, 64, 64, 1)
y_train = np.array(y_train)

config = {"conv_layer": 2, "first_conv_filter": 16,
           "pooling_size":3, "dense_layer":1,
           "dense_size": 32, "learning_rate": 0.001}

model = VoxelModel(config)
print(model.summary())
logdir = os.path.join("logs", "NAME")
tensorboard_callback = TensorBoard(logdir)
model.train(X_train, y_train, batch_size=32, epochs=10, callbacks=[tensorboard_callback])