from model import VoxelModel
import time
import numpy as np
import os
import pickle
from tensorflow.keras.callbacks import TensorBoard

DATASET = 'VANILLA'  # 'VANILLA' OR 'ADJUSTED'

if DATASET == 'VANILLA':
    pickle_in = open("data/X_train.pickle","rb")
    X_train = pickle.load(pickle_in)
    pickle_in = open("data/y_train.pickle","rb")
    y_train = pickle.load(pickle_in)
elif DATASET == 'ADJUSTED':
    pickle_in = open("data/X_train_adjusted.pickle", "rb")
    X_train = pickle.load(pickle_in)
    pickle_in = open("data/y_train_adjusted.pickle", "rb")
    y_train = pickle.load(pickle_in)
else:
    print('Dataset not specified!')

X_train = np.array(X_train).reshape(-1, 64, 64, 64, 1)
y_train = np.array(y_train)

configs = {"conv_layers": [1, 2, 3], "first_conv_filters": [16, 32, 64],
           "pooling_sizes": [2, 3], "dense_layers": [0, 1, 2], "dense_sizes": [32, 64, 128],
           "learning_rates": [0.001, 0.0001, 0.00001]}
config = 0
counter = 0
filepath = "checkpoint.txt"
file = open(filepath, "r")
checkpoint = int(file.read())
file.close()
for conv_layer in configs["conv_layers"]:
    for first_conv_filter in configs["first_conv_filters"]:
        for pooling_size in configs["pooling_sizes"]:
            for dense_layer in configs["dense_layers"]:
                for dense_size in configs["dense_sizes"]:
                    for learning_rate in configs["learning_rates"]:
                        counter += 1
                        if counter <= checkpoint:
                            continue
                        NAME = f"{conv_layer}-conv-{first_conv_filter}-fcv-{pooling_size}" \
                               f"-pool-{dense_layer}-dense-{dense_size}-size-{learning_rate}-lr-{int(time.time())}"
                        print(NAME)
                        config = {"conv_layer": conv_layer, "first_conv_filter": first_conv_filter,
                                   "pooling_size":pooling_size, "dense_layer":dense_layer,
                                   "dense_size": dense_size, "learning_rate": learning_rate}
                        print(NAME)
                        model = VoxelModel(config)
                        print(model.summary())
                        logdir = os.path.join("logs", NAME)
                        tensorboard_callback = TensorBoard(logdir)
                        model.train(X_train, y_train, batch_size=32, epochs=10, callbacks=[tensorboard_callback])
                        checkpoint += 1
                        file = open(filepath, "w")
                        file.write(str(checkpoint))
                        file.close()



