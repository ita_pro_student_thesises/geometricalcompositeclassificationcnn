import numpy as np
import pickle
from model import VoxelModel
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt

pickle_in = open("data/X_test.pickle","rb")
X_test = pickle.load(pickle_in)
pickle_in = open("data/y_test.pickle","rb")
y_test = pickle.load(pickle_in)

X_test = np.array(X_test)
X_test = X_test.reshape(-1, 64, 64, 64, 1)
y_test = np.array(y_test)

model = VoxelModel(config={}, use_existing_model=True)
model.load("trained_model_vanilla")
pred = model.predict(X_test)
pred_max = np.argmax(pred, axis=1)

cm = confusion_matrix(y_test, pred_max)

n_correct = 0
wrong_files = []
wrong_idx = []
for i in range(len(pred)):
    if pred_max[i] == y_test[i]:
        n_correct += 1
    else:
        wrong_idx.append(i)

#plot confusion matrix
def plot_cm(cm):

    fig, ax = plt.subplots(figsize=(8,3))
    im = ax.imshow(cm, cmap="binary")
    categories = ["shell-shaped", "complex", "profile-shaped", "tubular"]
    ax.set_xticks(np.arange(len(categories)))
    ax.set_yticks(np.arange(len(categories)))
    ax.set_yticklabels(categories)
    ax.set_xticklabels(categories)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",rotation_mode="anchor")
    ax.tick_params(labelsize=11, labelcolor="black")
    for tick in ax.get_xticklabels():
        tick.set_fontname("Arial")
    for tick in ax.get_yticklabels():
        tick.set_fontname("Arial")
    for i in range(len(categories)):
        for j in range(len(categories)):
            if cm[i, j] > 50:
                text = ax.text(j, i, cm[i, j],
                               ha="center", va="center", color="w", fontname="Arial",size=11)
            else:
                text = ax.text(j, i, cm[i, j],
                               ha="center", va="center", color="black", fontname="Arial", size=11)
    plt.show()


plot_cm(cm)
