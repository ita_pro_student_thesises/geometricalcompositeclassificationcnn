import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Conv3D, MaxPooling3D
import os

class VoxelModel:
    def __init__(self, config, loss=tf.keras.losses.SparseCategoricalCrossentropy(),
                 lr=0.001, use_existing_model=False):
        self.loss = loss
        self.lr = lr
        self.use_existing_model = use_existing_model
        self.model = None

        if not use_existing_model:

            self.model = Sequential([
                Conv3D(config["first_conv_filter"], 3, activation="relu", input_shape=(64, 64, 64, 1)),
                MaxPooling3D(pool_size=config["pooling_size"]),
                Dropout(0.3)
            ])
            for i in range(config["conv_layer"]-1):
                self.model.add(Conv3D(config["first_conv_filter"]*2**(i+1), 3, activation="relu"))
                self.model.add(MaxPooling3D(pool_size=config["pooling_size"]))
            self.model.add(Flatten()), # this converts our 3D feature maps to 1D feature vectors
            for i in range(config["dense_layer"]):
                self.model.add(Dense(config["dense_size"], activation="relu"))
                if i == 0:
                    self.model.add(Dropout(0.3))
            self.model.add(Dense(4, activation="softmax"))

            self.model.compile(loss=self.loss,
                          optimizer=tf.keras.optimizers.Adam(lr=config["learning_rate"]),
                          metrics=['accuracy'])

    def save(self, model_name):
        path = os.path.join(model_name)
        self.model.save(path)

    def load(self, model_name):
        path = os.path.join(model_name)
        self.model = tf.keras.models.load_model(path)

    def train(self, X_train, y_train, batch_size, epochs, callbacks):
        self.model.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, validation_split=0.1, shuffle=True, callbacks = callbacks)

    def predict(self, X_test):
        return self.model.predict(X_test)

    def summary(self):
        return self.model.summary()




