This repo contains TensorFlow neural network implementations for 3D data classification using three different approches:
- Voxel grids
- Point clouds
- Multi-views

**Example usage:**

To start hyperparameter optimisation using voxel grids:
1. Put pickled training data (X_train.pickle and y_train.pickle) in ./voxel/data OR Put categorically sorted STL files in ./STL_files and run ./voxel/preprocessing_voxelgrid.py
2. Run ./voxel/hyperparamtuning_voxel.py

To run tests on voxel data with pretrained models:
1. Put test data in data folders, as described above
2. Run ./voxel/test_voxel.py to create predictions and visualise a confusion matrix

The data already included in this repo are the original data used for training and the adjusted data set, as described in the paper.

**Known issues:**

Saving trained PointNet models currently not possible

**Requirements (pip install):**

1. numpy==1.23.0 (matrix operations)
2. tensorflow==2.8.0 (Machine Learning)
3. tensorboard==2.8.0 (visualize training process)
4. trimesh==3.9.20 (read CAD-data and create voxels)
5. pyntcloud==0.1.4 (create pointclouds)
6. matplotlib==3.4.2 (plot voxels and pointclouds)
7. scikit-learn==1.0 (create confusion matrix)
8. pdf2image==1.16.0 (convert pdf to png)




	