import tensorflow as tf
from tensorflow import keras
import numpy as np
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import pickle

model = tf.keras.models.load_model("test")
model.compile(
            loss="sparse_categorical_crossentropy",
            optimizer=keras.optimizers.Adam(learning_rate=0.001),
            metrics=["sparse_categorical_accuracy"],
        )

pickle_in = open("data/X_test.pickle", "rb")
X_test = pickle.load(pickle_in)
y_test = pickle.load(pickle_in)

X_test = np.array(X_test).swapaxes(1, 2)
y_test = np.array(y_test)

pred = model.predict(X_test)
pred_max = np.argmax(pred, axis=1)

cm = confusion_matrix(y_test, pred_max)

def plot_cm(cm):

    fig,ax = plt.subplots(figsize=(8,3))
    im = ax.imshow(cm,cmap="binary")
    categories = ["shell-shaped", "complex", "profile-shaped", "tubular"]
    ax.set_xticks(np.arange(len(categories)))
    ax.set_yticks(np.arange(len(categories)))
    ax.set_yticklabels(categories)
    ax.set_xticklabels(categories)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",rotation_mode="anchor")
    ax.tick_params(labelsize=11, labelcolor="black")
    for tick in ax.get_xticklabels():
        tick.set_fontname("Arial")
    for tick in ax.get_yticklabels():
        tick.set_fontname("Arial")
    for i in range(len(categories)):
        for j in range(len(categories)):
            if cm[i,j] >5:
                text = ax.text(j, i, cm[i, j],
                               ha="center", va="center", color="w", fontname="Arial",size=11)
            else:
                text = ax.text(j, i, cm[i, j],
                               ha="center", va="center", color="black", fontname="Arial", size=11)
    plt.show()

plot_cm(cm)
