import trimesh as tm
from pyntcloud import PyntCloud
import numpy as np
import os
import random
import pickle


DATASET = 'VANILLA'  # 'VANILLA' OR 'ADJUSTED'

if DATASET == 'VANILLA':
    DATADIR = "../STL_files/data"
elif DATASET == 'ADJUSTED':
    DATADIR = "../STL_files/data_adjusted"
else:
    print('Dataset not specified!')


def stl_to_pointcloud(train_test):
    CATEGORIES = os.listdir(DATADIR)
    data = []
    assignment = []
    swap01 = 0  # counter to check balanced number of rotations
    swap02 = 0  # counter to check balanced number of rotations

    for category in CATEGORIES:
        path = os.path.join(DATADIR, category, train_test)
        class_number = CATEGORIES.index(category)
        counter = 0
        for obj in os.listdir(path):
            counter += 1
            mesh = tm.load(os.path.join(path, obj))
            mesh.rezero()
            mesh.export("temp.obj")
            pointcloud = PyntCloud.from_file("temp.obj")
            pointcloud = pointcloud.get_sample("mesh_random", n=2048, rgb=False, normals=False, as_PyntCloud=False)
            points = np.array([pointcloud.x, pointcloud.y, pointcloud.z])

            if counter % 2 == 0 and counter % 3 != 0:  #rotate coordinates to prevent overfit
                points[[0, 1], :] = points[[1, 0], :]
                print("swap:0,1")
                swap01 += 1
            if counter % 3 == 0:
                points[[0, 2], :] = points[[2, 0], :]
                print("swap:0,2")
                swap02 += 1

            data.append([points, class_number])
            assignment.append(obj)
    return data, assignment


def stl_to_normalized_pointcloud(train_test):
    CATEGORIES = os.listdir(DATADIR)
    data = []
    assignment = []
    swap01 = 0  # counter to check balanced number of rotations
    swap02 = 0  # counter to check balanced number of rotations

    for category in CATEGORIES:
        path = os.path.join(DATADIR, category, train_test)
        class_number = CATEGORIES.index(category)
        counter = 0
        for obj in os.listdir(path):
            counter += 1
            mesh = tm.load(os.path.join(path, obj))
            mesh.rezero()
            max_extent = mesh.extents[np.argmax(mesh.extents)]  # check maximum length in geometry
            mesh.export("temp.obj")
            pointcloud = PyntCloud.from_file("temp.obj")
            pointcloud = pointcloud.get_sample("mesh_random", n=2048, rgb=False, normals=False, as_PyntCloud=False)
            points = np.array([pointcloud.x, pointcloud.y, pointcloud.z])
            points = points/max_extent  # normalize whole pointcloud

            if counter % 2 == 0 and counter % 3 != 0:  #rotate coordinates to prevent overfit
                points[[0, 1], :] = points[[1, 0], :]
                print("swap:0,1")
                swap01 += 1
            if counter%3 == 0:
                points[[0, 2], :] = points[[2, 0], :]
                print("swap:0,2")
                swap02 += 1

            data.append([points, class_number])
            assignment.append(obj)
    return data, assignment


def save_data(data, assignment, train_test):

    temp = list(enumerate(data))
    random.shuffle(temp)
    indices, data = zip(*temp)
    X = []
    y = []
    assignment = [indices, assignment]

    for features, label in data:
        X.append(features)
        y.append(label)

    if DATASET == 'VANILLA':
        X_filename = "./data/X_" + train_test + ".pickle"
        y_filename = "./data/y_" + train_test + ".pickle"
        assignment_filename = "./data/assignment_" + train_test + ".pickle"
    elif DATASET == 'ADJUSTED':
        X_filename = "./data/X_" + train_test + "_adjusted.pickle"
        y_filename = "./data/y_" + train_test + "_adjusted.pickle"
        assignment_filename = "./data/assignment_" + train_test + "_adjusted.pickle"

    pickle_out = open(X_filename,"wb")
    pickle.dump(X, pickle_out)
    pickle_out.close()

    pickle_out = open(y_filename,"wb")
    pickle.dump(y, pickle_out)
    pickle_out.close()

    pickle_out = open(assignment_filename, "wb")
    pickle.dump(assignment, pickle_out)
    pickle_out.close()


def get_categories():
    categories = os.listdir(DATADIR)
    return categories


training_data, train_assignment = stl_to_pointcloud("train")  # create training dataset
test_data, test_assignment = stl_to_pointcloud("test")  # create test dataset
save_data(training_data, train_assignment, "train")
save_data(test_data, test_assignment, "test")
