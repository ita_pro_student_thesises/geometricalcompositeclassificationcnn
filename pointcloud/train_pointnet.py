# PointNet Code from:
# https://keras.io/examples/vision/pointnet/

import os
import pickle
import tensorflow as tf
from model import PointNet
import trimesh as tm
import matplotlib.pyplot as plt
import numpy as np
from pyntcloud import PyntCloud
from sklearn.metrics import confusion_matrix
from tensorflow.keras.callbacks import TensorBoard
import csv

# load train and test datasets
DATASET = 'VANILLA'  # 'VANILLA' OR 'ADJUSTED'

if DATASET == 'VANILLA':
    pickle_in = open("data/X_train.pickle","rb")
    X_train = pickle.load(pickle_in)
    pickle_in = open("data/y_train.pickle","rb")
    y_train = pickle.load(pickle_in)
elif DATASET == 'ADJUSTED':
    pickle_in = open("data/X_train_adjusted.pickle", "rb")
    X_train = pickle.load(pickle_in)
    pickle_in = open("data/y_train_adjusted.pickle", "rb")
    y_train = pickle.load(pickle_in)
else:
    print('Dataset not specified!')

X_train=np.array(X_train).swapaxes(1, 2)
y_train=np.array(y_train)

# shuffle points in each pointcloud
for i in range(len(X_train)):
    X_train[i] = tf.random.shuffle(X_train[i])
    X_train[i] = np.array(X_train[i])

#create PointNet model and start training
model = PointNet().model
NAME = "batch32,epochs15,dropout03V3"
logdir = os.path.join("logs", NAME)
tensorboard_callback = TensorBoard(logdir)
model.fit(X_train, y_train, batch_size=32, epochs=15, validation_split=0.1, shuffle=True, callbacks=[tensorboard_callback])
# end PointNet training

# Model saving currently throws error because of OrthogonalRegularizer class
#model.save('test')

# Test PointNet
pickle_in = open("data/X_test.pickle", "rb")
X_test = pickle.load(pickle_in)
y_test = pickle.load(pickle_in)

X_test = np.array(X_test).swapaxes(1, 2)
y_test = np.array(y_test)

pred = model.predict(X_test)
pred_max = np.argmax(pred, axis=1)

cm = confusion_matrix(y_test, pred_max)


def plot_cm(cm):
    fig, ax = plt.subplots(figsize=(8,3))
    im = ax.imshow(cm, cmap="binary")
    categories = ["shell-shaped", "complex", "profile-shaped", "tubular"]
    ax.set_xticks(np.arange(len(categories)))
    ax.set_yticks(np.arange(len(categories)))
    ax.set_yticklabels(categories)
    ax.set_xticklabels(categories)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")
    ax.tick_params(labelsize=11, labelcolor="black")
    for tick in ax.get_xticklabels():
        tick.set_fontname("Arial")
    for tick in ax.get_yticklabels():
        tick.set_fontname("Arial")
    for i in range(len(categories)):
        for j in range(len(categories)):
            if cm[i, j] > 5:
                text = ax.text(j, i, cm[i, j],
                               ha="center", va="center", color="w", fontname="Arial",size=11)
            else:
                text = ax.text(j, i, cm[i, j],
                               ha="center", va="center", color="black", fontname="Arial", size=11)
    plt.show()


plot_cm(cm)
