import tensorflow as tf
import numpy as np
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import pickle

model = tf.keras.models.load_model("trained_model_vanilla")
model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001), loss="SparseCategoricalCrossentropy", metrics=["accuracy"])

pickle_in = open("data/pickled_test/X_test_0.pickle", "rb")
X_test_0 = pickle.load(pickle_in)
pickle_in = open("data/pickled_test/X_test_1.pickle", "rb")
X_test_1 = pickle.load(pickle_in)
pickle_in = open("data/pickled_test/X_test_2.pickle", "rb")
X_test_2 = pickle.load(pickle_in)
pickle_in = open("data/pickled_test/y_test.pickle", "rb")
y_test = pickle.load(pickle_in)

X_test_0 = np.array(X_test_0)
X_test_1 = np.array(X_test_1)
X_test_2 = np.array(X_test_2)
y_test = np.array(y_test)

X_test_0 = X_test_0.reshape(-1,100,100,1)
X_test_1 = X_test_1.reshape(-1,100,100,1)
X_test_2 = X_test_2.reshape(-1,100,100,1)

X_test_0 = X_test_0/255
X_test_1 = X_test_1/255
X_test_2 = X_test_2/255

X_test = {"0": X_test_0, "1": X_test_1, "2": X_test_2}

pred = model.predict(X_test)
pred_max = np.argmax(pred, axis=1)

cm = confusion_matrix(y_test, pred_max)

def plot_cm(cm):

    fig,ax = plt.subplots(figsize=(8,3))
    im = ax.imshow(cm,cmap="binary")
    categories = ["shell-shaped", "complex", "profile-shaped", "tubular"]
    ax.set_xticks(np.arange(len(categories)))
    ax.set_yticks(np.arange(len(categories)))
    ax.set_yticklabels(categories)
    ax.set_xticklabels(categories)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",rotation_mode="anchor")
    ax.tick_params(labelsize=11, labelcolor="black")
    for tick in ax.get_xticklabels():
        tick.set_fontname("Arial")
    for tick in ax.get_yticklabels():
        tick.set_fontname("Arial")
    for i in range(len(categories)):
        for j in range(len(categories)):
            if cm[i,j] >5:
                text = ax.text(j, i, cm[i, j],
                               ha="center", va="center", color="w", fontname="Arial",size=11)
            else:
                text = ax.text(j, i, cm[i, j],
                               ha="center", va="center", color="black", fontname="Arial", size=11)
    plt.show()

plot_cm(cm)
