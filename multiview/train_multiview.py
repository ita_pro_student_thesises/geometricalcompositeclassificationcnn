import pickle
from model import MultiviewModel
import time
import numpy as np
import os
from tensorflow.keras.callbacks import TensorBoard

DATASET = 'VANILLA'  # 'VANILLA' OR 'ADJUSTED'

if DATASET == 'VANILLA':
    pickle_in = open("data/pickled/X_train_0.pickle","rb")
    X_train_0 = pickle.load(pickle_in)
    pickle_in = open("data/pickled/X_train_1.pickle","rb")
    X_train_1 = pickle.load(pickle_in)
    pickle_in = open("data/pickled/X_train_2.pickle","rb")
    X_train_2 = pickle.load(pickle_in)
    pickle_in = open("data/pickled/y_train.pickle","rb")
    y_train = pickle.load(pickle_in)
elif DATASET == 'ADJUSTED':
    pickle_in = open("data/pickled_adjusted/X_train_0.pickle", "rb")
    X_train_0 = pickle.load(pickle_in)
    pickle_in = open("data/pickled_adjusted/X_train_1.pickle", "rb")
    X_train_1 = pickle.load(pickle_in)
    pickle_in = open("data/pickled_adjusted/X_train_2.pickle", "rb")
    X_train_2 = pickle.load(pickle_in)
    pickle_in = open("data/pickled_adjusted/y_train.pickle", "rb")
    y_train = pickle.load(pickle_in)
else:
    print('Dataset not specified!')

X_train_0 = np.array(X_train_0)
X_train_1 = np.array(X_train_1)
X_train_2 = np.array(X_train_2)
y_train = np.array(y_train)

X_train_0 = X_train_0.reshape(-1,100,100,1)
X_train_1 = X_train_1.reshape(-1,100,100,1)
X_train_2 = X_train_2.reshape(-1,100,100,1)

X_train_0 = X_train_0/255
X_train_1 = X_train_1/255
X_train_2 = X_train_2/255

X_train = {"0": X_train_0, "1": X_train_1, "2": X_train_2}

NAME = "final model"
print(NAME)
config = {"conv_layer": 2, "first_conv_filter": 64,
           "pooling_size":3, "dense_layer":1,
           "dense_size": 32, "learning_rate": 0.001}
print(NAME)
model = MultiviewModel(config)
print(model.summary())

logdir = os.path.join("logs", NAME)
tensorboard_callback = TensorBoard(logdir)

model.train(X_train, y_train, batch_size=16, epochs=20, callbacks=[tensorboard_callback])
model.model.save(NAME)
