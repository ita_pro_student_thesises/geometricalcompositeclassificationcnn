import pickle
from model import MultiviewModel
import time
import numpy as np
import os
from tensorflow.keras.callbacks import TensorBoard

DATASET = 'VANILLA'  # 'VANILLA' OR 'ADJUSTED'

if DATASET == 'VANILLA':
    pickle_in = open("data/pickled/X_train_0.pickle","rb")
    X_train_0 = pickle.load(pickle_in)
    pickle_in = open("data/pickled/X_train_1.pickle","rb")
    X_train_1 = pickle.load(pickle_in)
    pickle_in = open("data/pickled/X_train_2.pickle","rb")
    X_train_2 = pickle.load(pickle_in)
    pickle_in = open("data/pickled/y_train.pickle","rb")
    y_train = pickle.load(pickle_in)
elif DATASET == 'ADJUSTED':
    pickle_in = open("data/pickled_adjusted/X_train_0.pickle", "rb")
    X_train_0 = pickle.load(pickle_in)
    pickle_in = open("data/pickled_adjusted/X_train_1.pickle", "rb")
    X_train_1 = pickle.load(pickle_in)
    pickle_in = open("data/pickled_adjusted/X_train_2.pickle", "rb")
    X_train_2 = pickle.load(pickle_in)
    pickle_in = open("data/pickled_adjusted/y_train.pickle", "rb")
    y_train = pickle.load(pickle_in)
else:
    print('Dataset not specified!')

X_train_0 = np.array(X_train_0)
X_train_1 = np.array(X_train_1)
X_train_2 = np.array(X_train_2)
y_train = np.array(y_train)

X_train_0 = X_train_0.reshape(-1,100,100,1)
X_train_1 = X_train_1.reshape(-1,100,100,1)
X_train_2 = X_train_2.reshape(-1,100,100,1)

X_train_0 = X_train_0/255
X_train_1 = X_train_1/255
X_train_2 = X_train_2/255

X_train = {"0": X_train_0, "1": X_train_1, "2": X_train_2}

configs = {"batch_sizes": [64, 32, 16], "conv_layers": [2, 3], "first_conv_filters": [64, 32, 16],
          "pooling_sizes": [2, 3], "dense_layers":[2, 1, 0], "dense_sizes":[64, 32], "learning_rates":[0.001, 0.0001]}
config = 0
counter = 0
filepath = "checkpoint.txt"
file = open(filepath, "r")
checkpoint = int(file.read())
file.close()
for batch_size in configs["batch_sizes"]:
    for conv_layer in configs["conv_layers"]:
        for first_conv_filter in configs["first_conv_filters"]:
            for pooling_size in configs["pooling_sizes"]:
                for dense_layer in configs["dense_layers"]:
                    for dense_size in configs["dense_sizes"]:
                        for learning_rate in configs["learning_rates"]:
                            counter += 1
                            if counter <= checkpoint:
                                continue
                            NAME = f"{batch_size}-batch-{conv_layer}-conv-{first_conv_filter}-fcv-{pooling_size}" \
                                   f"-pool-{dense_layer}-dense-{dense_size}-size-{learning_rate}-lr-{int(time.time())}"
                            print(NAME)
                            config = {"conv_layer": conv_layer, "first_conv_filter": first_conv_filter,
                                       "pooling_size":pooling_size, "dense_layer":dense_layer,
                                       "dense_size": dense_size, "learning_rate": learning_rate}
                            print(NAME)
                            model = MultiviewModel(config)
                            print(model.summary())

                            logdir = os.path.join("logs", NAME)
                            tensorboard_callback = TensorBoard(logdir)

                            model.train(X_train, y_train, batch_size=batch_size, epochs=20, callbacks=[tensorboard_callback])
                            checkpoint += 1
                            file = open(filepath, "w")
                            file.write(str(checkpoint))
                            file.close()