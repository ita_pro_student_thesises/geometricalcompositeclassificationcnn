import os
import cv2
import matplotlib.pyplot as plt
import pickle
import random

DATASET = 'TRAIN'  # 'TRAIN', 'TEST' OR 'ADJUSTED'

if DATASET == 'TRAIN':
    DATADIR = "data/png"
elif DATASET == 'TEST':
    DATADIR = "data/png_test"
elif DATASET == 'ADJUSTED':
    DATADIR = "data/png_adjusted"
else:
    print('Dataset not specified!')

CATEGORIES = os.listdir(DATADIR)
data = []

for category in CATEGORIES:
    path = os.path.join(DATADIR, category)
    class_number = CATEGORIES.index(category)
    for obj in os.listdir(path):
        print(obj)
        temp = []
        for img in os.listdir(os.path.join(path,obj)):
            img_array = cv2.imread(os.path.join(path,obj,img),cv2.IMREAD_GRAYSCALE)
            img_array = cv2.resize(img_array, (100,100))
            #img_array = img_array/255
            temp.append(img_array)
            random.shuffle(temp)
        data.append([temp,class_number])

X_test_0 = []
X_test_1 = []
X_test_2 = []
y_test = []

random.shuffle(data)

for features, label in data:

    X_test_0.append(features[0])
    X_test_1.append(features[1])
    X_test_2.append(features[2])
    y_test.append(label)

if DATASET == 'TRAIN':
    X0_filename = "data/pickled/X_train_0.pickle"
    X1_filename = "data/pickled/X_train_1.pickle"
    X2_filename = "data/pickled/X_train_2.pickle"
    y_filename = "data/pickled/y_train.pickle"
elif DATASET == 'TEST':
    X0_filename = "data/pickled_test/X_test_0.pickle"
    X1_filename = "data/pickled_test/X_test_1.pickle"
    X2_filename = "data/pickled_test/X_test_2.pickle"
    y_filename = "data/pickled_test/y_test.pickle"
elif DATASET == 'ADJUSTED':
    X0_filename = "data/pickled_adjusted/X_train_0.pickle"
    X1_filename = "data/pickled_adjusted/X_train_1.pickle"
    X2_filename = "data/pickled_adjusted/X_train_2.pickle"
    y_filename = "data/pickled_adjusted/y_train.pickle"

pickle_out=open(X0_filename,"wb")
pickle.dump(X_test_0, pickle_out)
pickle_out.close()

pickle_out=open(X1_filename,"wb")
pickle.dump(X_test_1, pickle_out)
pickle_out.close()

pickle_out=open(X2_filename,"wb")
pickle.dump(X_test_2, pickle_out)
pickle_out.close()

pickle_out=open(y_filename,"wb")
pickle.dump(y_test, pickle_out)
pickle_out.close()