import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D
import os


class MultiviewModel:
    def __init__(self, config, loss=tf.keras.losses.SparseCategoricalCrossentropy(),
                 lr=0.001, use_existing_model=False):
        self.loss = loss
        self.lr = lr
        self.use_existing_model = use_existing_model
        self.model = None

        if not use_existing_model:
            Input0 = keras.Input(shape=(100, 100, 1), name="0")
            Input1 = keras.Input(shape=(100, 100, 1), name="1")
            Input2 = keras.Input(shape=(100, 100, 1), name="2")

            x0 = Conv2D(config["first_conv_filter"], 3, activation="relu")(Input0)
            x1 = Conv2D(config["first_conv_filter"], 3, activation="relu")(Input1)
            x2 = Conv2D(config["first_conv_filter"], 3, activation="relu")(Input2)

            x0 = MaxPooling2D(pool_size=config["pooling_size"])(x0)
            x1 = MaxPooling2D(pool_size=config["pooling_size"])(x1)
            x2 = MaxPooling2D(pool_size=config["pooling_size"])(x2)

            x0 = Dropout(0.2)(x0)
            x1 = Dropout(0.2)(x1)
            x2 = Dropout(0.2)(x2)

            for i in range(config["conv_layer"]-1):
                x0 = Conv2D(config["first_conv_filter"]*2**(i+1), 3, activation="relu")(x0)
                x1 = Conv2D(config["first_conv_filter"]*2**(i+1), 3, activation="relu")(x1)
                x2 = Conv2D(config["first_conv_filter"]*2**(i+1), 3, activation="relu")(x2)
                x0 = MaxPooling2D(pool_size=config["pooling_size"])(x0)
                x1 = MaxPooling2D(pool_size=config["pooling_size"])(x1)
                x2 = MaxPooling2D(pool_size=config["pooling_size"])(x2)
            x = keras.layers.concatenate([x0, x1, x2])
            x = Flatten()(x)
            for i in range(config["dense_layer"]):
                x = Dense(config["dense_size"], activation="relu")(x)
                if i == 0:
                    x = Dropout(0.1)(x)
            prediction = Dense(4, name="prediction", activation="softmax")(x)
            self.model = keras.Model(
                inputs=[Input0, Input1, Input2],
                outputs=prediction
            )
            self.model.compile(loss=self.loss,
                          optimizer=tf.keras.optimizers.Adam(lr=config["learning_rate"]),
                          metrics=['accuracy'])

    def save(self, model_name):
        path = os.path.join(model_name)
        self.model.save(path)

    def load(self, model_name):
        path = os.path.join(model_name)
        self.model = tf.keras.models.load_model(path)

    def train(self, X_train, y_train, batch_size, epochs, callbacks):
        self.model.fit(X_train, y_train, batch_size=batch_size, epochs=epochs, validation_split=0.1, shuffle=True, callbacks = callbacks)

    def predict(self, X_test):
        return self.model.predict(X_test)

    def summary(self):
        return self.model.summary()




